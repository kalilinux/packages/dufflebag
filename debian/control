Source: dufflebag
Section: misc
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Vcs-Browser: https://gitlab.com/kalilinux/packages/dufflebag
Vcs-Git: https://gitlab.com/kalilinux/packages/dufflebag.git
Homepage: https://github.com/BishopFox/dufflebag
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/BishopFox/dufflebag

Package: dufflebag
Architecture: all
Depends: ${misc:Depends},
         golang-any,
         golang-github-aws-aws-sdk-go-dev,
         golang-github-deckarep-golang-set-dev,
         golang-lukechampine-blake3-dev (>= 1.1.5),
         make,
         sensible-utils,
         zip
Built-Using: ${misc:Built-Using}
Description: Search exposed EBS volumes for secrets (program)
 Dufflebag is a tool that searches through public Elastic Block Storage (EBS)
 snapshots for secrets that may have been accidentally left in.
 .
 The tool is organized as an Elastic Beanstalk ("EB", not to be confused
 with EBS) application, and definitely won't work if you try to run it
 on your own machine.
 .
 Dufflebag has a lot of moving pieces because it's fairly nontrivial
 to actually read EBS volumes in practice. You have to be in an AWS
 environment, clone the snapshot, make a volume from the snapshot, attach
 the volume, mount the volume, etc... This is why it's made as an Elastic
 Beanstalk app, so it can automagically scale up or down however much you
 like, and so that the whole thing can be easily torn down when you're
 done with it.
